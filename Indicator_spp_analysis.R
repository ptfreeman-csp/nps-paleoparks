
library(indicspecies)
library(tidyverse)

#This script is used to run an indicator species analysis. It uses the breakpoint files that are outputs from the cluster analysis script
#(and manually modified to include expert opinion)
#Note that I manually added a column for dataset.id because it wasn't in the breakfile. Probably worth adding this during cluster analysis


#Point directory towards the breakpoint files 
mydir="./breakpoint_files_harm_exp_opinion/"

#List breakpoint files 
breakpoint_files<-list.files(path=mydir, pattern=".csv", full.names = TRUE)

#Reads in individual .csv files and creates a single dataframe 
breakpoints<-plyr::ldply(breakpoint_files, read.csv)

#List the pollen data files 
site_data<-list.files("./site_pollen_updated_ages_harmonized/")



allsites_indcspp<-c()


# Looping through sites and comparing each consecutive cluster for each site
for(i in 1:length(site_data)){

#Get the site info from the pollen data to help track the site 
site_info<-read.csv(paste("./site_pollen_updated_ages_harmonized/",site_data[i], sep="")) %>% 
  dplyr::select(dataset.id, site.name) %>% 
  distinct()

#Read in teh harmonized pollen data 
datperc_wide<-read.csv(paste("./site_pollen_updated_ages_harmonized/", site_data[[i]], sep="")) %>% 
  dplyr::select(-X)

#Filter the breakpoint object for the focal site 
site_breakpoint<-filter(breakpoints, site.name==site_info$site.name & dataset.id==site_info$dataset.id) %>% 
 mutate(clust_num=cumsum(breakpt_bottom))



#The following loop loops through each pair of consecutive clusters and identifies the 'indicator' species for each cluster as well as 
#a statistic to determine if it is a 'significant' indicator 
ind_spp<-c()


for(a in min(site_breakpoint$clust_num + 1):max(site_breakpoint$clust_num)){
  
ind_breakpoints<-site_breakpoint %>% 
  filter(clust_num>=a-1 & clust_num<=a)


ind_data<-filter(datperc_wide, age>=min(ind_breakpoints$age) & age<=max(ind_breakpoints$age)) %>% 
  dplyr::select(12:ncol(.))

#The 'multipatt' function is the one that identifies the indicator species  
indicspp_mod<-multipatt(ind_data, ind_breakpoints$clust_num, duleg=TRUE)

#This chunk of code cleans things up, only keeps the important things, and creates a database with all sites/clusters 
ind_spp[[a]]<-indicspp_mod$sign %>%
  rownames_to_column() %>%
  rename(species=rowname) %>%
  filter(p.value<=0.05) %>%
  arrange(desc(stat)) %>%
  arrange(index) %>%
  mutate(site.name=site_info$site.name, dataset.id=site_info$dataset.id) %>% 
  rename(s.1=2, s.2=3) %>% 
  mutate(cluster=ifelse(s.1==1, a-1, a), clust_compare=paste("clust", a-1, "vs", a, sep=""))
  
  
}


allsites_indcspp[[i]]<-do.call("rbind", ind_spp)


}


allsites_indspp_df<-do.call("rbind", allsites_indcspp) %>% 
  dplyr::select(-index)

  
write.csv(allsites_indspp_df, "eastern_paleo_allsites_indicator_spp.csv")

miller44<-indicspp_mod$sign %>%
  rownames_to_column() 

write.csv(miller44, "miller44_ind_spp.csv")
