
library(tidyverse)
library(sf)
library(neotoma)
library(terrainr)

options(scipen = 100)


#This setting takes care of a function that stopped working after a package update
sf::sf_use_s2(FALSE)

nps_boundary<-sf::st_read(dsn="nps_boundary") %>% 
  st_transform(4269) %>%  #these two lines take care of a 'self-intersection' that happens on line 49 if I don't have the buffer of 0. See this post: https://gis.stackexchange.com/questions/163445/getting-topologyexception-input-geom-1-is-invalid-which-is-due-to-self-intersec
  st_buffer(., dist = 0) %>%
  rename_all(tolower)


# ggplot()+
#   geom_sf(data=nps_boundary)

nps_eastern<-st_crop(nps_boundary, xmin= -100, ymin= -14.28316, xmax= 145.7318, ymax=68.65539)




park_types<-nps_eastern %>% 
  data.frame() %>% 
  dplyr::select(unit_name, unit_type) %>% 
  distinct() %>% 
  rename(park_name=unit_name)

write.csv(park_types, "park_types.csv")

st_write(nps_eastern, "eastern_parks/nps_eastern_parks.shp") #check to see if it cropped correctly in QGIS 

eastern_park_paleo_records<-c()

for(i in 1:length(nps_eastern$unit_name)) {
  
  park<-nps_eastern$unit_name[i]
  x<-filter(nps_eastern, unit_name==park)
  
  x_buffer<-add_bbox_buffer(data=x, distance=50000, distance_unit = "meters")
  
  x_bbox<-c(st_bbox(x_buffer))
  
x_pollen_records <-get_dataset(loc = x_bbox, datasettype = 'pollen')

site<-c()
dataset.id<-c()
description<-c()

for(a in 1:length(x_pollen_records)){
  
  site[a]<-x_pollen_records[[a]]$site.data$site.name
  dataset.id[a]<-x_pollen_records[[a]]$dataset.meta$dataset.id
  description[a]<-x_pollen_records[[a]]$site.data$description
  
}

if(is.null(site)==TRUE){
  x_allsites<-data.frame(park_name=park, site.name=NA, dataset.id=NA, description=NA )
} else {
 x_allsites<-data.frame(park_name=park, site.name=site, dataset.id=dataset.id, description=description)
}


eastern_park_paleo_records[[i]]<-x_allsites


}

eastern_park_records<-do.call("bind_rows", eastern_park_paleo_records) %>% 
  distinct() %>% 
  drop_na() %>% 
  filter(park_name!="North Country National Scenic Trail", park_name!="Appalachian National Scenic Trail") %>% 
  left_join(park_types) %>% 
  rename(paleo_site=site.name, nps_unit_type=unit_type)

#write.csv(eastern_park_records, "eastern_park_paleo_records_list_v2.csv")

park_records_summary<-eastern_park_records %>% 
  drop_na() %>% 
  group_by(park_name, nps_unit_type) %>% 
  summarize(n_records=length(park_name))

write.csv(park_records_summary, "park_records_summary_v2.csv")


####Downloading pollen data and creating a database of lat/longs ####


#This line downloads the records and returns them in list form   
nps_pollen.downloads <-get_download(eastern_park_records$dataset.id, verbose = FALSE)

#This line compiles the downloads into a dataframe
nps_pollen_data<- compile_downloads(nps_pollen.downloads)


additional_sites_from_SC<-get_download(c(772,15660), verbose=FALSE)

additional_sites_pollen_data<-compile_downloads(additional_sites_from_SC)




nps_pollen_locations<-nps_pollen_data%>% 
  dplyr::select(site.name, lat, long, .id)%>%
  distinct()%>% 
  rename(dataset.id=".id", dataset.name=site.name)%>% 
  mutate(lat=round(lat, digits=2), long=round(long, digits=2), data_type="pollen", dataset.id=as.numeric(dataset.id)) %>% 
  right_join(eastern_park_records) %>% 
  dplyr::select(park_name, dataset.name, dataset.id, data_type, description, lat, long) %>% 
  arrange(park_name)


write.csv(nps_pollen_locations, "eastern_park_paleo_records_and_locations_v2.csv")


#### Reading in a shapefile of the pollen locations that I made in QGIS ####

pollen_shp<-st_read("./eastern_parks_paleo_points_shp") %>% 
  st_transform(4269)


#This chunk of code creates a column that indicates whether the site is located in a park or not. 
sites_in_park<-st_intersection(nps_eastern, pollen_shp)



sites_park_df<-sites_in_park %>% 
  data.frame() %>% 
  dplyr::select(park_name, state, region, dataset.na, dataset.id,  -geometry) %>% 
  rename(dataset.name=dataset.na) %>% 
  mutate(in_park=1) 

write.csv(sites_park_df, "paleo_sites_in_park.csv")

#This calculates the distance of each site to a park 
pollen_shp$dist_park<-st_distance(pollen_shp, st_union(nps_eastern))


#Create a shapefile for visualizations
st_write(pollen_shp, "./eastern_parks_paleo_points_shp/points_shp_dist_park.shp")

#Create a dataframe of the sites and distance to each park 
dist_park<-pollen_shp %>% 
  data.frame() %>% 
  dplyr::select(park_name, dataset.na, dataset.id, dist_park, -geometry) %>% 
  mutate(dist_park=round(as.numeric(dist_park),digits=2)) 

#This filters it to our 10km buffer (10000m)
dist_park_10000<-dist_park %>% 
  filter(dist_park<=10000) 

write.csv(dist_park_10000, "pollen_sites_10km.csv")




##### Creating final list of sites based on list from Shelley and Gregor ####


site_list<-read.csv("final_paleo_site_list.csv") %>% 
  rename_all(tolower) %>% 
  dplyr::select(site, dataset.id) %>% 
  drop_na(dataset.id)

nps_final_paleo_records<-eastern_park_records %>% 
  filter(dataset.id %in% site_list$dataset.id) %>% 
  dplyr::select(paleo_site, dataset.id) %>% 
  distinct()
  


nps_pollen_final_downloads<-get_download(c(nps_final_paleo_records$dataset.id, 772, 15660, 3559), verbose = FALSE) #Added 3 dataset.ids that Shelley and Gregor identified manually

nps_pollen_data<- compile_downloads(nps_pollen_final_downloads)



saveRDS(nps_pollen_data, "nps_eastern_allsites_pollen_data_v2.RDS")

nps_pollen_data<-readRDS("nps_eastern_allsites_pollen_data_v2.RDS")


saveRDS(nps_pollen_final_downloads, "nps_eastern_final_site_download_list_v2.RDS")





#### Extracting chrono controls ####

chrono_controls<-get_chroncontrol(nps_pollen_final_downloads)


saveRDS(all_chrono_controls, "chrono_control_list_nps_eastern_paleo_sites_v2.rds")

portage_marsh_core2_3559<-chrono_controls$`3559`$chron.control

write.csv(portage_marsh_core2_3559, file="portage_marsh_core2_3559.csv")



#### Code to create text files of depths for each site for bacon modeling ####

# change site names to get rid of spaces and slashes
nps_pollen_data$site_name_clean <- str_replace_all(nps_pollen_data$site.name, " ", "_")
nps_pollen_data$site_name_clean <- str_replace_all(nps_pollen_data$site_name_clean, "\\/", "_")
nps_pollen_data$sitename_id <- paste(nps_pollen_data$site_name_clean, nps_pollen_data$.id, sep = "_")

# loop through each unique combo of site name x ID and export a text file of just the depths
for(i in 1:length(unique(nps_pollen_data$sitename_id))){ # for each combo of site name x ID
  site_id <- unique(nps_pollen_data$sitename_id)[i] # get the site x ID
  nps_pollen_data %>% filter(sitename_id == site_id) %>% # filter the data to just that site x ID
    select(depth) %>%  # just get the depth column
    write.table(paste(site_id, "_depth.txt", sep = ""), row.names = FALSE, col.names = FALSE) # save as a text file
}
